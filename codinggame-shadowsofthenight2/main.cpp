#include <iostream>
#include <string>
#include <vector>
#include <algorithm>

using namespace std;

/**
 * Auto-generated code below aims at helping you parse
 * the standard input according to the problem statement.
 **/
int main()
{
    int W; // width of the building.
    int H; // height of the building.
    int N; // maximum number of turns before game over.
    int X0;
    int Y0;
#ifdef editor
    W = 5;
    H = 16;
    N = 80;
    X0 = 1;
    Y0 = 5;
#else
    cin >> W >> H; cin.ignore();
    cin >> N; cin.ignore();
    cin >> X0 >> Y0; cin.ignore();
#endif
    
    // game loop
    while (1) {
        string bombDir; // Current distance to the bomb compared to previous distance (COLDER, WARMER, SAME or UNKNOWN)
#ifdef editor
        bombDir = "SAME";
#else
        cin >> bombDir; cin.ignore();
#endif
        
        // Write an action using cout. DON'T FORGET THE "<< endl"
        // To debug: cerr << "Debug messages..." << endl;
        
        //5 16 80 1 5 SAME
        cerr << W << " " << H << " " << N << " " << X0 << " " << Y0 << " " << bombDir << endl;
        
        cout << "0 0" << endl;
    }
}
